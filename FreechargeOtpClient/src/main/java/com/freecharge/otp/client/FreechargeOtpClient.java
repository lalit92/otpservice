package com.freecharge.otp.client;

import com.fasterxml.jackson.core.type.TypeReference;


import com.freecharge.otp.client.util.ClientDetails;
import com.freecharge.otp.client.util.HttpClientEntity;
import com.freecharge.otp.client.util.HttpClientFactory;
import com.freecharge.otp.client.util.HttpUtil;
import com.freecharge.otp.exception.InternalClientException;
import com.freecharge.otp.exception.OtpServiceException;
import com.freecharge.otp.exception.code.OtpServiceExceptionCode;
import com.freecharge.otp.request.VerifyOtpRequest;
import com.freecharge.otp.request.GenerateOtpRequest;
import com.freecharge.otp.response.GenerateOtpResponse;
import com.freecharge.otp.response.VerifyOtpResponse;


import com.freecharge.otp.util.OtpServiceUri;
import com.snapdeal.payments.sdmoney.exceptions.SDMoneyException;
import com.snapdeal.payments.sdmoney.service.model.ExceptionParser;
import com.snapdeal.payments.sdmoney.service.model.ServiceResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.mina.http.api.HttpMethod;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Timer;


@Slf4j
public class FreechargeOtpClient implements IOtpService{

    private ClientDetails clientDetails;
    private HttpClientEntity httpClientEntity;
    private HttpClientFactory httpClientFactory = HttpClientFactory.getInstance();
    private Timer timer;



    public FreechargeOtpClient(String serviceHostUrl, String clientName, String clientKey, String xAuthToken)
            throws Exception {
        ClientDetails details = new ClientDetails();
        details.setUrl(serviceHostUrl);
        details.setClientName(clientName);
        details.setClientKey(clientKey);
        details.setXAuthToken(xAuthToken);
        this.clientDetails = details;
        intializeHttpClient(details);
    }

    public FreechargeOtpClient(String serviceHostUrl, String clientId, String clientKey){

        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setUrl(serviceHostUrl);
        clientDetails.setClientKey(clientKey);
        this.clientDetails = clientDetails;
    }


    public <T> GenerateOtpResponse generateOtp(GenerateOtpRequest<T> request) {

        GenerateOtpResponse response = null;
        OtpServiceException exception = new OtpServiceException();
        try {
            String completeUri = createCompleteUrl(OtpServiceUri.GENERATE_OTP);
             response = processPostRequest(completeUri, request, new TypeReference<GenerateOtpResponse>() {
            });
        } catch (InternalClientException e) {
            log.error("Internal Exception Occoured: {} while processing request {}",e,request);
            exception.setErrorCode(OtpServiceExceptionCode.INTERNAL_CLIENT_EXCEPTION.getErrorCode());
            exception.setErrorMessage(OtpServiceExceptionCode.INTERNAL_CLIENT_EXCEPTION.getErrorMessage());
        }
        response.setException(exception);
        return response;
    }

    public <T,R> VerifyOtpResponse<R> verifyOtp(VerifyOtpRequest<T> request) {

        VerifyOtpResponse response = null;
        OtpServiceException exception = new OtpServiceException();
        String completerUri = createCompleteUrl(OtpServiceUri.VERIFY_OTP);
        try {
            response = processPostRequest(completerUri, request,new TypeReference<VerifyOtpResponse<R>>() {});
        } catch (InternalClientException e) {
            log.error("Internal Exception Occoured: {} while processing request {}",e,request);
            exception.setErrorCode(OtpServiceExceptionCode.INTERNAL_CLIENT_EXCEPTION.getErrorCode());
            exception.setErrorMessage(OtpServiceExceptionCode.INTERNAL_CLIENT_EXCEPTION.getErrorMessage());

        }
        return response;

    }



    private void intializeHttpClient(ClientDetails clientDetails) throws UnrecoverableKeyException,
            KeyManagementException, KeyStoreException, NoSuchAlgorithmException, CertificateException,
            FileNotFoundException, IOException {
        this.httpClientEntity = httpClientFactory.createHttpClient(clientDetails);
    }





    private <T, R> R processPostRequest(String requestUri, T request, TypeReference<R> typeReference)
            throws  InternalClientException {

        R response = (R) HttpUtil.getInstance().processHttpRequest(requestUri,
                typeReference, request, HttpMethod.POST, httpClientEntity.getHttpClient(), clientDetails);
        return response;
    }


    private <T, R> R processGetRequest(String requestUri, T request, TypeReference<R> typeReference)
            throws SDMoneyException, InternalClientException {

        R response = (R) HttpUtil.getInstance().processHttpRequest(requestUri,
                typeReference, request, HttpMethod.GET, httpClientEntity.getHttpClient(), clientDetails);

        return response;
    }


    private String createCompleteUrl(String relativeUrl) {
        return clientDetails.getUrl() + relativeUrl;
    }










/*    public static void main(String[] args) {

        AddBankDetailsRequest bankDetails = new AddBankDetailsRequest();

        GenerateOtpRequest<AddBankDetailsRequest> generateOtpRequest = new GenerateOtpRequest<AddBankDetailsRequest>(null,null);
        generateOtpRequest.setPurpose(Purpose.ADD_BANK_ACNT_BENF_OTP);
        generateOtpRequest.setRequest(bankDetails);

        VerifyOtpRequest<AddBankDetailsRequest> verifyOtpRequest = new VerifyOtpRequest(null,null,null,null,null);
        verifyOtpRequest.setRequest(bankDetails);
        verifyOtpRequest.setPurpose(Purpose.ADD_BANK_ACNT_BENF_OTP);
        System.out.println("success");



        FreechargeOtpClient client =  new FreechargeOtpClient();


        GenerateOtpResponse generateOtpResponse = client.generateOtp(generateOtpRequest);


        VerifyOtpResponse<Object> verifyOtpResponse = client.verifyOtp(verifyOtpRequest);

     //   Object response = verifyOtpResponse.getResponse();


    }*/



}
