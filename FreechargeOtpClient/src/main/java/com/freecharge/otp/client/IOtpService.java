package com.freecharge.otp.client;


import com.freecharge.otp.request.VerifyOtpRequest;
import com.freecharge.otp.request.GenerateOtpRequest;
import com.freecharge.otp.response.GenerateOtpResponse;
import com.freecharge.otp.response.VerifyOtpResponse;

public interface IOtpService {

    public <T> GenerateOtpResponse generateOtp(GenerateOtpRequest<T> request);

    public <T,R > VerifyOtpResponse<R> verifyOtp(VerifyOtpRequest<T> request);





}
