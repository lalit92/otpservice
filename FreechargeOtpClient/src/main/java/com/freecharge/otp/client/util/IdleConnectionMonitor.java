package com.freecharge.otp.client.util;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.util.TimerTask;

/**
 * Monitor thread to close all the expired and idle connection.
 * Expired Connection are decided from the parameter timeToLive and has been set
 * during connection establishment as now + timeToLive
 * Idle connection limit has been set to 1 minute.
 * Also see PoolingHttpClientConnectionManager for more information.
 * 
 * @author anamika
 *
 */
@Slf4j
public class IdleConnectionMonitor extends TimerTask {

   /**
    * Rate at which metric will get logged.
    */
   public static final long PUBLISH_RATE = 10 * 1000;

   public static final long MONITOR_RATE = 30 * 1000;

   /**
    * Thread name to be set from the configuration per client else default name
    * get published.
    */
   @Getter
   @Setter
   private String monitorName = "ConnectionMonitor";

   @Setter
   private PoolingHttpClientConnectionManager connMgr;

   @Override
   public void run() {
      Thread.currentThread().setName(monitorName);
      log.debug("Cleaning expired connection by " + monitorName + " IdleConnectionManager");
      //connMgr.closeExpiredConnections();
      publishStatus();
   }

   /**
    * Scheduled function to get the status of pooled connection in a metric.
    */
   public void publishStatus() {
      log.info("Connection Status :" + connMgr.getTotalStats());
      log.info(monitorName + ".max_connection : " + connMgr.getTotalStats().getMax());
      log.info(monitorName + ".available_connection : " + connMgr.getTotalStats().getAvailable());
      log.info(monitorName + ".pending_connection : " + connMgr.getTotalStats().getPending());
      log.info(monitorName + ".leased_connection : " + connMgr.getTotalStats().getLeased());
   }
}