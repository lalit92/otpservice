package com.freecharge.otp.client.util;

import com.snapdeal.payments.sdmoney.service.model.RequestHeaders;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

public class SystemInformationFetcher {

	public static void setApplicationInformationInHeader(
			Map<String, String> requestHeader) {
		InetAddress ip;
		String hostName = null, hostIpAddress = null, hostLoggedUserName = null, requestId = null;
		long timeStamp = System.currentTimeMillis();
		try {
			ip = InetAddress.getLocalHost();
			hostName = ip.getHostName();
			hostIpAddress = ip.getHostAddress();
			hostLoggedUserName = System.getenv().get("LOGNAME");
			requestId = UUID.randomUUID().toString();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		requestHeader.put(RequestHeaders.APP_CLIENT_NAME.getName(), hostName);
		requestHeader.put(RequestHeaders.APP_CLIENT_IP_ADDRESS.getName(),
				hostIpAddress);
		requestHeader.put(RequestHeaders.APP_LOGGED_USER.getName(),
				hostLoggedUserName);
		requestHeader.put(RequestHeaders.APP_CLIENT_TIMESTAMP.getName(),
				String.valueOf(timeStamp));
		requestHeader.put(RequestHeaders.APP_REQUEST_ID.getName(), requestId);

		SystemInformationFetcher sf = new SystemInformationFetcher();
		sf.setServiceClientNameVersion(requestHeader);
	}

	private void setServiceClientNameVersion(Map<String, String> requestHeader) {
		String clientVersion="";
		String clientName="";
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(
				"META-INF/maven/com.snapdeal.payments/SDMoneyJavaClient/pom.properties");
		Properties prop = new Properties();
		try {
			prop.load(is);
			clientVersion = prop.getProperty("version");
			clientName = prop.getProperty("artifactId");

		} catch (Exception e) {
		}

		requestHeader.put(RequestHeaders.SERVICE_CLIENT_VERSION.getName(),clientVersion);
		requestHeader.put(RequestHeaders.SERVICE_CLIENT_NAME.getName(), clientName);
	}
}
