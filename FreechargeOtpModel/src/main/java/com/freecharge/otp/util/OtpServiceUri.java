package com.freecharge.otp.util;

public class OtpServiceUri {


    public static final String GENERATE_OTP = "/generateOtp";

    public static final String VERIFY_OTP = "/verifyOtp";

    public static final String RESEND_OTP = "/resendOtp";



}
