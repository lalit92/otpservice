package com.freecharge.otp.request;

import lombok.Data;

@Data
public class ResendOtpRequest {

    String jwtToken;
    String otpId;
    ResendOtpRequestChannel channel;
}
