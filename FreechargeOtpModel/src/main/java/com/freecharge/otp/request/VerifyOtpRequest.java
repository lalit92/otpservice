package com.freecharge.otp.request;

import com.freecharge.otp.enums.Purpose;
import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotBlank;

@Data
public class VerifyOtpRequest<T> {

    @NonNull
    @NotBlank
    private String jwtToken;

    @NonNull
    @NotBlank
    String otpId;

    @NotBlank
    @NonNull
    String otp;

    @NonNull
    Purpose purpose;

    @NonNull
    T request;
}
