package com.freecharge.otp.request;

import com.freecharge.otp.enums.Purpose;
import lombok.Data;
import lombok.NonNull;

@Data
public class GenerateOtpRequest<T> {

    @NonNull
    private String jwtToken;

    @NonNull
    private Purpose purpose;

    private T request;


}


