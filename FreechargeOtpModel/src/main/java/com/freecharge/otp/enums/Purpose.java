package com.freecharge.otp.enums;

import com.snapdeal.payments.sdmoney.service.model.AddBankDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.UpdateCappingLimitsForUserRequest;
import com.snapdeal.payments.sdmoney.service.model.UpdateUnverifiedBankDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.UpdateUnverifiedBankDetailsResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import com.freecharge.otp.response.AddBankDetailsResponse;

@RequiredArgsConstructor
public enum Purpose {

    ADD_BANK_ACNT_BENF_OTP(AddBankDetailsRequest.class,AddBankDetailsResponse.class),

    EDIT_TXN_CAPPING_OTP(UpdateCappingLimitsForUserRequest.class, Void.class),

    EDIT_BANK_ACNT_BENF_OTP(UpdateUnverifiedBankDetailsRequest.class,UpdateUnverifiedBankDetailsResponse.class);

    @NonNull
    Class requestClass;
    @NonNull
    Class responseClass;

    public Class getRequestClass() {
        return requestClass;
    }
}
