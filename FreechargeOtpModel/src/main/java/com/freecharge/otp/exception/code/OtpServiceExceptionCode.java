package com.freecharge.otp.exception.code;

import lombok.Getter;

public enum  OtpServiceExceptionCode {

    HTTP_OK                                    ("OTP-200", OtpServiceExceptionMessage.SUCCESS),
    INTERNAL_CLIENT_EXCEPTION                  ("OTP-101", OtpServiceExceptionMessage.INTERNAL_CLIENT_EXCEPTION),
    INVALID_REQUEST_TYPE                       ("OTP-102", OtpServiceExceptionMessage.INVALID_REQUEST_TYPE),
    INVALID_RESPONSE_TYPE                      ("OTP-103", OtpServiceExceptionMessage.INVALID_RESPONSE_TYPE),

    OTP_VALIDATION_FAILED                      ("OTP-104",OtpServiceExceptionMessage.OTP_VALIDATION_FAILED),
    IMS_IS_DOWN                                ("OTP-105",OtpServiceExceptionMessage.IMS_IS_DOWN),
    OTP_SERVICE_IS_DOWN                        ("OTP-106",OtpServiceExceptionMessage.INTERNAL_CLIENT_EXCEPTION),

    AUTHORIZATION_FALIURE                      ("OTP-107",OtpServiceExceptionMessage.AUTHORIZATION_FAILURE);


    @Getter
    String errorCode;
    @Getter
    String errorMessage;

    OtpServiceExceptionCode(String errorCode,String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}
