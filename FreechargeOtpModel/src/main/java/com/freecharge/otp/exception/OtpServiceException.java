package com.freecharge.otp.exception;

import lombok.Data;

@Data
public class OtpServiceException extends RuntimeException{
    String errorCode;
    String errorMessage;
}
