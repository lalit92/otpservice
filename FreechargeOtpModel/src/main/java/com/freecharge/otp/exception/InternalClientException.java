package com.freecharge.otp.exception;

public class InternalClientException extends Throwable {



    public InternalClientException() {
    }

    public InternalClientException(String message) {
        super(message);
    }

    public InternalClientException(String message, Throwable e) {

    }

}
