package com.freecharge.otp.exception.code;

public class OtpServiceExceptionMessage {


    public static final String SUCCESS = "Request successfully processed";
    public static final String INVALID_REQUEST_TYPE = "Invalid Request";
    public static final String AUTHORIZATION_FAILURE = "You are not authorised to perform this action";
    public static final String OTP_VALIDATION_FAILED = "Otp validation failed";
    public static final String INVALID_RESPONSE_TYPE = "Invalid Response";
    public static final String INTERNAL_CLIENT_EXCEPTION = "Internal client exception occoured !! Please retry";

    public static final String IMS_IS_DOWN = "IMS service is down";
    public static final String WALLET_SERVICE_DOWN = "Wallet service is down";




}
