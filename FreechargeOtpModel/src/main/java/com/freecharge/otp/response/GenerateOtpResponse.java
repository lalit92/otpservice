package com.freecharge.otp.response;

import com.freecharge.otp.exception.OtpServiceException;
import lombok.Data;

@Data
public class GenerateOtpResponse {

    private String otpId;

    private OtpServiceException exception;


}
