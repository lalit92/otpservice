package com.freecharge.otp.response;

import com.freecharge.otp.exception.OtpServiceException;
import lombok.Data;

@Data
public class VerifyOtpResponse<R> {

    Boolean isOtpVerified;
    R response;
    OtpServiceException exception;
}
