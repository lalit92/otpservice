package com.freecharge.otp.common.beans;


import com.snapdeal.ims.client.IOTPServiceClient;
import com.snapdeal.ims.client.IUserServiceClient;
import com.snapdeal.ims.client.impl.OTPClientServiceImpl;
import com.snapdeal.ims.client.impl.UserServiceClientImpl;
import com.snapdeal.ims.utils.ClientDetails;
import com.snapdeal.payments.sdmoney.client.BankDetailsStoreClient;
import com.snapdeal.payments.sdmoney.client.SDMoneyClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ClientConfiguration {

    @Autowired
    private WalletClientConfiguration walletClientConfiguration;

    @Autowired
    private IMSClientConfiguration imsClientConfiguration;



    @Bean
    public IOTPServiceClient imsOtpClient() throws Exception {
        ClientDetails.init(imsClientConfiguration.getIp(), imsClientConfiguration.getPort(),
                imsClientConfiguration.getSlaveUrl(), imsClientConfiguration.getSlavePort(),
                imsClientConfiguration.getClientKey(), imsClientConfiguration.getClientId(),
                imsClientConfiguration.getTimeout());
        return new OTPClientServiceImpl();
    }

    @Bean
    public IUserServiceClient imsClient() throws Exception {
        return new UserServiceClientImpl();
    }

    @Bean
    public SDMoneyClient sdMoneyClient() throws Exception {

        com.snapdeal.payments.sdmoney.client.utils.ClientDetails clientDetails = new com.snapdeal.payments.sdmoney.client.utils.ClientDetails();

        clientDetails.setClientName(walletClientConfiguration.getClientName());
        clientDetails.setClientKey(walletClientConfiguration.getClientKey());
        clientDetails.setUrl(walletClientConfiguration.getUrl());
        clientDetails.setConnectTimeout(walletClientConfiguration.getConnectTimeout());
        clientDetails.setSocketTimeout(walletClientConfiguration.getSocketTimeout());
        clientDetails.setConnectionRequestTimeout(walletClientConfiguration.getConnectionRequestTimeout());
        clientDetails.setXAuthToken(walletClientConfiguration.getXAuthToken());
        clientDetails.setKeepAlive(walletClientConfiguration.getKeepAlive());
        return new SDMoneyClient(clientDetails);



    }

    @Bean
    public BankDetailsStoreClient bankStoreClient() throws Exception {

        com.snapdeal.payments.sdmoney.client.utils.ClientDetails clientDetails = new com.snapdeal.payments.sdmoney.client.utils.ClientDetails();

        clientDetails.setClientName(walletClientConfiguration.getClientName());
        clientDetails.setClientKey(walletClientConfiguration.getClientKey());
        clientDetails.setUrl(walletClientConfiguration.getUrl());
        clientDetails.setConnectTimeout(walletClientConfiguration.getConnectTimeout());
        clientDetails.setSocketTimeout(walletClientConfiguration.getSocketTimeout());
        clientDetails.setConnectionRequestTimeout(walletClientConfiguration.getConnectionRequestTimeout());
        clientDetails.setXAuthToken(walletClientConfiguration.getXAuthToken());
        clientDetails.setKeepAlive(walletClientConfiguration.getKeepAlive());
        return new BankDetailsStoreClient(clientDetails);



    }
}


