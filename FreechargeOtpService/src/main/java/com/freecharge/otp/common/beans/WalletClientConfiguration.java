package com.freecharge.otp.common.beans;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "otp.client.wallet")
public class WalletClientConfiguration {

    private String clientName;
    private String clientKey;
    private String url;
    private String xAuthToken;
    private int connectionRequestTimeout;
    private int connectTimeout;
    private int socketTimeout;
    private long keepAlive;
}
