package com.freecharge.otp.common.beans;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Data
@Component
@ConfigurationProperties(prefix = "otp.client.ims")
public class IMSClientConfiguration {

    private String ip;
    private String port;
    private String clientKey;
    private String clientId;
    private int timeout;
    private String slaveUrl;
    private String slavePort;

}
