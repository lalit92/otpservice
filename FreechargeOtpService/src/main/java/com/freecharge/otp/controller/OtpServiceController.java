package com.freecharge.otp.controller;


import com.freecharge.otp.api.OtpService;
import com.freecharge.otp.enums.Purpose;
import com.freecharge.otp.exception.OtpServiceException;
import com.freecharge.otp.exception.code.OtpServiceExceptionCode;
import com.freecharge.otp.request.GenerateOtpRequest;
import com.freecharge.otp.request.VerifyOtpRequest;
import com.freecharge.otp.response.GenerateOtpResponse;
import com.freecharge.otp.response.VerifyOtpResponse;
import com.freecharge.otp.util.CheckSumCalculator;
import com.freecharge.otp.util.OtpServiceUri;
import com.snapdeal.ims.enums.OTPPurpose;
import com.snapdeal.ims.exception.ServiceException;
import com.snapdeal.ims.request.GenerateOTPRequestV2;
import com.snapdeal.ims.request.VerifyOTPRequestV2;
import com.snapdeal.ims.response.GenerateOTPResponse;
import com.snapdeal.ims.response.VerifyOTPResponse;
import com.snapdeal.payments.sdmoney.service.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
public class OtpServiceController  {


    @Autowired
    private OtpService otpService;



    @RequestMapping(value = OtpServiceUri.GENERATE_OTP , produces = "application/json" , method = RequestMethod.POST)
    public <T> GenerateOtpResponse generateOtp(GenerateOtpRequest<T> request) throws IllegalAccessException, InstantiationException {

        GenerateOtpResponse response = new GenerateOtpResponse();
        OtpServiceException otpServiceException = new OtpServiceException();
        if (isValidRequest(request.getRequest(),request.getPurpose())){
            GenerateOTPRequestV2 generateOTPRequest = getImsGenerateOtpRequest(request);
            GenerateOTPResponse generateOTPResponse = null;
            try {
                generateOTPResponse = otpService.sendOTP(generateOTPRequest);
                otpServiceException.setErrorCode(OtpServiceExceptionCode.HTTP_OK.getErrorCode());
                otpServiceException.setErrorMessage(OtpServiceExceptionCode.HTTP_OK.getErrorMessage());
            } catch (ServiceException e) {
                log.error("Ims exception occoured: {}",e);
                otpServiceException.setErrorCode(OtpServiceExceptionCode.IMS_IS_DOWN.getErrorCode());
                otpServiceException.setErrorMessage(OtpServiceExceptionCode.IMS_IS_DOWN.getErrorMessage());

            }
            response.setOtpId(generateOTPResponse.getOtpId());

        }else {
            otpServiceException.setErrorCode(OtpServiceExceptionCode.INVALID_REQUEST_TYPE.getErrorCode());
            otpServiceException.setErrorMessage(OtpServiceExceptionCode.INVALID_REQUEST_TYPE.getErrorMessage());
        }
        response.setException(otpServiceException);
        return response;
    }


    public <T,R > VerifyOtpResponse<R> verifyOtp(VerifyOtpRequest<T> request){
        Boolean isOtpVerified = Boolean.FALSE;
        VerifyOTPResponse imsVerifyOtpResponse;
        VerifyOtpResponse verifyOtpResponse = new VerifyOtpResponse();
        OtpServiceException verifyOtpException = new OtpServiceException();

        VerifyOTPRequestV2 imsverifyOtpRequest = getImsVerifyOtpRequest(request);

        try {
            log.info("Sending verify otp request {} to IMS", imsverifyOtpRequest);
            imsVerifyOtpResponse = otpService.verifyOTP(imsverifyOtpRequest);
            isOtpVerified = imsVerifyOtpResponse.isOtpVerified();
            verifyOtpResponse.setIsOtpVerified(isOtpVerified);
        } catch (ServiceException e) {
            log.error("Got exception from IMS: {} ", e);
            verifyOtpException.setErrorCode(OtpServiceExceptionCode.IMS_IS_DOWN.getErrorCode());
            verifyOtpException.setErrorMessage(OtpServiceExceptionCode.IMS_IS_DOWN.getErrorMessage());
            verifyOtpResponse.setResponse(Void.class);
            verifyOtpResponse.setIsOtpVerified(Boolean.FALSE);
            return verifyOtpResponse;
        }
        if (isOtpVerified && isValidRequest(request.getRequest(), request.getPurpose()) ){

            switch (request.getPurpose()){

                case ADD_BANK_ACNT_BENF_OTP:
                    AddBankDetailsRequest bankDetailsRequest = (AddBankDetailsRequest) request.getRequest();
                    AddBankDetailsResponse addBankDetailsResponse = otpService.addBankDetails(bankDetailsRequest);
                    verifyOtpResponse.setResponse(addBankDetailsResponse);
                    break;

                case EDIT_BANK_ACNT_BENF_OTP:
                    UpdateUnverifiedBankDetailsRequest updateBankDetails =
                            (UpdateUnverifiedBankDetailsRequest) request.getRequest();

                    UpdateUnverifiedBankDetailsResponse updateBankDetailsResponse =
                            otpService.setBankDetailsVerified(updateBankDetails);

                    verifyOtpResponse.setResponse(updateBankDetailsResponse);
                    break;

                case EDIT_TXN_CAPPING_OTP:
                    UpdateCappingLimitsForUserRequest updateCapping =
                            (UpdateCappingLimitsForUserRequest) request.getRequest();

                    otpService.updateCappingLimitsForUser(updateCapping);
                    verifyOtpResponse.setResponse(Void.class);
                    break;

                default:

                    verifyOtpResponse.setException(verifyOtpException);
            }

        }else if (!isOtpVerified){
            verifyOtpException.setErrorCode(OtpServiceExceptionCode.OTP_VALIDATION_FAILED.getErrorCode());
            verifyOtpException.setErrorMessage(OtpServiceExceptionCode.OTP_VALIDATION_FAILED.getErrorMessage());
            verifyOtpResponse.setResponse(Void.class);

        }else {
            verifyOtpException.setErrorCode(OtpServiceExceptionCode.INVALID_REQUEST_TYPE.getErrorCode());
            verifyOtpException.setErrorMessage(OtpServiceExceptionCode.INVALID_REQUEST_TYPE.getErrorMessage());
        }
        verifyOtpResponse.setException(verifyOtpException);
        return verifyOtpResponse;
    }

    private <T> VerifyOTPRequestV2 getImsVerifyOtpRequest(VerifyOtpRequest<T> request) {

        VerifyOTPRequestV2 requestV2 = new VerifyOTPRequestV2();
        requestV2.setJwt(request.getJwtToken());
        requestV2.setOtp(request.getOtp());
        requestV2.setOtpId(request.getOtpId());
        requestV2.setContentHash(CheckSumCalculator.generateSHA256CheckSum(request.getRequest().toString()));

        return requestV2;
    }




    private <T> GenerateOTPRequestV2 getImsGenerateOtpRequest(GenerateOtpRequest<T> request) {

        GenerateOTPRequestV2 imsRequest = new GenerateOTPRequestV2();
        imsRequest.setJwt(request.getJwtToken());
        imsRequest.setPurpose(OTPPurpose.forValue(request.getPurpose().name()));
        imsRequest.setContentHash(CheckSumCalculator.generateSHA256CheckSum(request.toString()));
        return imsRequest;
    }


    private <T> boolean isValidRequest(T request,Purpose purpose){
        boolean isValid = false;
        if (purpose.getRequestClass().equals(request.getClass())){
            isValid = true;
        }
        return isValid;
    }

   /* public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        OtpServiceController controller = new OtpServiceController();
        GenerateOtpRequest request = new GenerateOtpRequest();
        request.setRequest(new AddBankDetailsRequest());
        request.setPurpose(Purpose.ADD_BANK);
        GenerateOtpResponse generateOtpResponse = controller.generateOtp(request);
        System.out.println(generateOtpResponse);
    }*/

}
