package com.freecharge.otp.app;


import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

public class PropertyLoader {

    @Bean
    public static PropertyPlaceholderConfigurer properties(){
        PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
        ClassPathResource[] resources = new ClassPathResource[ ]{
                new ClassPathResource("conf/log.properties"),
                new ClassPathResource("conf/application.properties"),
                new ClassPathResource("conf/db.properties"),
                new ClassPathResource("conf/log.properties"),
                new ClassPathResource("conf/api.properties")
        };
        ppc.setLocations( resources );
        ppc.setIgnoreUnresolvablePlaceholders( true );
        return ppc;
    }
}