package com.freecharge.otp.api;

import com.freecharge.otp.external.service.IMSOtpService;
import com.freecharge.otp.external.service.WalletService;
import com.snapdeal.payments.sdmoney.api.BankDetailsStoreService;

public interface OtpService extends WalletService,IMSOtpService {
}
