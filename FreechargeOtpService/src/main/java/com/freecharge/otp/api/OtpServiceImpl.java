package com.freecharge.otp.api;

import com.freecharge.otp.external.service.IMSOtpService;
import com.freecharge.otp.external.service.WalletService;
import lombok.experimental.Delegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("OtpServiceImpl")
public class OtpServiceImpl implements OtpService{

    @Autowired
    @Delegate
    private WalletService walletService;

    @Autowired
    @Delegate
    private IMSOtpService imsOtpService;

}
