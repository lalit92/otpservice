package com.freecharge.otp.external.service;

import com.snapdeal.payments.sdmoney.exceptions.SDMoneyException;
import com.snapdeal.payments.sdmoney.service.model.*;

public interface WalletService {

    AddBankDetailsResponse addBankDetails(AddBankDetailsRequest var1) throws SDMoneyException;

    UpdateUnverifiedBankDetailsResponse setBankDetailsVerified(UpdateUnverifiedBankDetailsRequest var1) throws SDMoneyException;

    void updateCappingLimitsForUser(UpdateCappingLimitsForUserRequest updateCappingLimitsForUserRequest)
            throws SDMoneyException;
}
