package com.freecharge.otp.external.impl;

import com.freecharge.otp.external.service.WalletService;
import com.snapdeal.payments.sdmoney.client.BankDetailsStoreClient;
import com.snapdeal.payments.sdmoney.client.SDMoneyClient;
import com.snapdeal.payments.sdmoney.exceptions.SDMoneyException;
import com.snapdeal.payments.sdmoney.service.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WalletServiceImpl implements WalletService {


    @Autowired
    private SDMoneyClient sdMoneyClient;


    @Autowired
    private BankDetailsStoreClient bankDetailsStoreClient;


    @Override
    public AddBankDetailsResponse addBankDetails(AddBankDetailsRequest var1) throws SDMoneyException {
        return bankDetailsStoreClient.addBankDetails(var1);
    }

    @Override
    public UpdateUnverifiedBankDetailsResponse setBankDetailsVerified(UpdateUnverifiedBankDetailsRequest var1) throws SDMoneyException {
        return null;
    }


    @Override
    public void updateCappingLimitsForUser(UpdateCappingLimitsForUserRequest updateCappingLimitsForUserRequest) throws SDMoneyException {

    }
}
