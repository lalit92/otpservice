package com.freecharge.otp.external.service;

import com.snapdeal.ims.exception.HttpTransportException;
import com.snapdeal.ims.exception.ServiceException;
import com.snapdeal.ims.request.GenerateOTPRequestV2;
import com.snapdeal.ims.request.ResendOTPRequest;
import com.snapdeal.ims.request.VerifyOTPRequestV2;
import com.snapdeal.ims.response.GenerateOTPResponse;
import com.snapdeal.ims.response.VerifyOTPResponse;

public interface IMSOtpService {

    GenerateOTPResponse sendOTP(GenerateOTPRequestV2 var1) throws ServiceException, HttpTransportException;

    VerifyOTPResponse verifyOTP(VerifyOTPRequestV2 var1) throws ServiceException, HttpTransportException;

    GenerateOTPResponse resendOTP(ResendOTPRequest var1) throws ServiceException, HttpTransportException;

}
