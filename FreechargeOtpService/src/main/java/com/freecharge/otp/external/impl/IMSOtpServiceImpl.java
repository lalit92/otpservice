package com.freecharge.otp.external.impl;

import com.freecharge.otp.external.service.IMSOtpService;
import com.snapdeal.ims.client.IOTPServiceClient;
import com.snapdeal.ims.exception.HttpTransportException;
import com.snapdeal.ims.exception.ServiceException;
import com.snapdeal.ims.request.GenerateOTPRequestV2;
import com.snapdeal.ims.request.ResendOTPRequest;
import com.snapdeal.ims.request.VerifyOTPRequestV2;
import com.snapdeal.ims.response.GenerateOTPResponse;
import com.snapdeal.ims.response.VerifyOTPResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IMSOtpServiceImpl implements IMSOtpService {

    @Autowired
    private IOTPServiceClient imsOtpServiceClient;


    @Override
    public GenerateOTPResponse sendOTP(GenerateOTPRequestV2 var1) throws ServiceException, HttpTransportException {
        return imsOtpServiceClient.sendOTP(var1);
    }

    @Override
    public VerifyOTPResponse verifyOTP(VerifyOTPRequestV2 var1) throws ServiceException, HttpTransportException {
        return imsOtpServiceClient.verifyOTP(var1);
    }

    @Override
    public GenerateOTPResponse resendOTP(ResendOTPRequest var1) throws ServiceException, HttpTransportException {
        return imsOtpServiceClient.resendOTP(var1);
    }
}
